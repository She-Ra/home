---
title: About me
subtitle: Data Science | Python | R | SQL
comments: false
---

I'm Mac, I like R and Python. I have a PhD in physics, an undergrad in pure maths. I've worked across academia and the private sector. I'm interested in:
* linear algebra
* quantum mechanics
* quantum comptuing
* statistics
* Python
* R
* data science / machine learning / mind control from afar using all the data (<- joke)
* planning and being organised (wish I knew about project methodologies before I started my PhD!)
* coffee
* dilbert 
* PhD Comics.

This blog documents my progress as I work through R / Python / mathematics / physics courses and projects.

Please check out my projects!


