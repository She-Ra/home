---
title: "Worked solutions for Quantum Computation and Quantum Information"
date: 2018-02-09
tags: ['quantum computing', 'worked solutions']
---

I work through the problem sets in "Quantum Computation and Quantum Information" 10th Anniversary Edition by M.A. Nielson and I.L. Chuang (2010) Cambridge University Press.

Please [download the PDF here](https://gitlab.com/She-Ra/Quantum-Computation-and-Quantum-Information/blob/master). I can't yet publish my work to the web because I'm not sure how (or if) `\usepackage` can be used in conjunction with a html output. For example there are some commands like `\ket{}` in the `physics` latex package I use - these work in pdf but not yet for a html format.

This is the first time I've used latex with R Markdown, it's cool - [check out my markdown file](https://gitlab.com/She-Ra/Quantum-Computation-and-Quantum-Information/blob/master/Worked-exercises.md) if you'd like to try it yourself.